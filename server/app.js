const express = require('express')

const app = express()
const cors = require('cors')
/**** */
const leboncoin = require('leboncoin-api');
var search = new leboncoin.Search()
    .setPage(1)

    .setCategory("Ventes immobilières")
  
    .setDepartment("aisne")
    // .addSearchExtra("price", {min: 1500, max: 2000}) // will add a range of price


search.run().then(function (data) {
    console.log(data.page); // the current page
    console.log(data.nbResult); // the number of results for this search
    console.log(data.results); // the array of results
    data.results[0].getDetails().then(function (details) {
        console.log(details); // the item 0 with more data such as description, all images, author, ...
    }, function (err) {
        console.error(err);
    });
    data.results[0].getPhoneNumber().then(function (phoneNumer) {
        console.log(phoneNumer); // the phone number of the author if available
    }, function (err) {
        console.error(err); // if the phone number is not available or not parsable (image -> string) 
    });
}, function (err) {
    console.error(err);
    console.error(err);
});

/**** */




app.use(cors
    ({
            origine: 'localhost:8000'
    }))        

app.get('/', (req, res) => {

    const produits = [
        {id: 1, nbr_pieces: 3, prix: 150},
        { id: 2, nbr_pieces: 5, prix: 230 },
        {id: 3, nbr_pieces: 1, prix: 100}
    ]
   
    res.json(produits)

    

})

app.listen(8000, () => console.log('app test is running on port : 8000'))
